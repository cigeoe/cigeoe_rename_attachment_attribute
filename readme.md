# CIGeoE Rename Attachment Attribute

Rename attachment attribute of selected field on all features

# Installation

For QGIS 3:

There are two methods to install the plugin:

- Method 1:

  1- Copy the folder “cigeoe_rename_attachment_attribute” to folder:

  ```console
  [drive]:\Users\[user]\AppData\Roaming\QGIS\QGIS3\profiles\default\python\plugins
  ```

  2 - Start QGis and go to menu "Plugins" and select "Manage and Install Plugins"

  3 - Select “CIGeoE Rename Attachment Attribute”

  4 - After confirm the operation the plugin will be available in toolbar

- Method 2:

  1 - Compress the folder “cigeoe_rename_attachment_attribute” (.zip).

  2 - Start QGis and go to "Plugins" and select “Manage and Install Plugins”

  3 - Select “Install from Zip” (choose .zip generated in point 1) and then select "Install Plugin"

# Usage

1 - In the "layers panel" select the desired layer for edition.

![ALT](./images/image01.png)

2 - Clicking on the plugin icon opens a widget with one combo box and a text line.

![ALT](./images/image02.png)

2.1 - Combo box (1): select the attribute whose value will change (type "string")

2.2 - Text line (2): enter the new value (mandatory). This value must have a path followed by attribute names between two dollar signs ($) - only values between these simbols will be considered ($fid$_$designacao$$$% outputs “fid” “designacao”).

When the "text line" field has the new value, press enter.

## Example

1 - After selecting the desired layer and put it in edition mode, select the plugin "CIGeoE Rename Attachment Attribute".

![ALT](./images/image03.png)

2 - In the "combo box", select the attribute whose value will be updated.

![ALT](./images/image04.png)

3 - Put the new value in the "text line" field (all the fields names must be inside $ signs).

![ALT](./images/image05.png)

4 - After press enter, a blue message pops up if it succeeds, or a "message box" with information appears on screen.

![ALT](./images/image06.png)

5 - Click on the plugin button to deactivate it.

6 - Results:

 - initial:
 ![ALT](./images/image07.png)

 - final
 ![ALT](./images/image08.png)


# Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

# License

[AGPL 3.0](https://www.gnu.org/licenses/agpl-3.0.en.html)
